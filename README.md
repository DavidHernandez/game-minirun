# Videogame MiniRun

This code was prepared for a Minirun at [Devscola](https://devscola.org).

## Licenses

All the code wrote specifically for this project is under the GPL3 License.

### Assets

- src/assets/space3.png under MIT license, coming from the (photonstorm/phaser-examples)[https://github.com/photonstorm/phaser-examples] repository.
- src/assets/phaser.png under MIT license, coming from the (photonstorm/phaser-examples)[https://github.com/photonstorm/phaser-examples] repository.
- src/assets/background-day.png under MIT license, coming from (sourabhv/FlapPyBird)[https://github.com/sourabhv/FlapPyBird] repository.
- src/assets/pipe-green.png under MIT license, coming from (sourabhv/FlapPyBird)[https://github.com/sourabhv/FlapPyBird] repository.
- src/assets/bird.png derived from the original file under MIT license, coming from (sourabhv/FlapPyBird)[https://github.com/sourabhv/FlapPyBird] repository.
- src/assets/logo.png derived from the original file under MIT license, coming from (sourabhv/FlapPyBird)[https://github.com/sourabhv/FlapPyBird] repository.
- src/assets/wing.ogg under MIT license, coming from (sourabhv/FlapPyBird)[https://github.com/sourabhv/FlapPyBird] repository.
- src/assets/wing.wav under MIT license, coming from (sourabhv/FlapPyBird)[https://github.com/sourabhv/FlapPyBird] repository.
